//
//  HttpClientTests.swift
//  HttpClientTests
//
//  Created by Tran Kien on 7/1/17.
//  Copyright © 2017 Kien Tran. All rights reserved.
//

import XCTest
@testable import HttpClient

class HttpClientTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        

        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let client = RESTFulClient(baseURL: URL.init(string: "http://google.com")!)
        client.append(logging: ConsoleHttpLogging())
        
        client.request(relativeURL: "", method: .get) { (error, data, response) in
            
        }
        
    Thread.sleep(forTimeInterval: 5)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
