//
//  RESTFulClient+XML.swift
//  HttpClient
//
//  Created by Tran Kien on 7/1/17.
//  Copyright © 2017 Kien Tran. All rights reserved.
//

import Foundation
import SWXMLHash
import Alamofire

enum BackendError: Error {
    case network(error: Error) // Capture any underlying Error from the URLSession API
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case xmlSerialization(error: Error)
    case objectSerialization(reason: String)
}

extension XMLIndexer {
    func asNSDictionary() -> AnyObject! {
        
        let dic = NSMutableDictionary()
        if self.children.isEmpty {
            if let text = self.element?.text {
                //                dic.setValue(text, forKey: self.element!.name)
                return text as AnyObject!
            }
            else {
                return nil
            }
            
        }
        else if self.children.count == 1 {
            let firstChild = self.children.first!
            
            if let element = firstChild.element {
                dic.setValue(firstChild.asNSDictionary(), forKey: element.name)
            }
            else {
                assertionFailure("cannot get element")
            }
        }
        else {
            
            for child in self.children {
                
                if let element = child.element {
                    let key = element.name
                    
                    if let object = dic[key] {
                        var array = object as? [Any]
                        
                        if (array == nil) {
                            array = [object]
                        }
                        
                        if let childDictionary = child.asNSDictionary() {
                            array!.append(childDictionary)
                            
                            _ = childDictionary as? NSDictionary
                        }
                        
                        dic.setValue(array, forKey: key)
                        
                    }
                    else {
                        dic.setValue(child.asNSDictionary(), forKey: element.name)
                    }
                }
                else {
                    assertionFailure("cannot get element")
                }
            }
        }
        
        return dic.copy() as AnyObject!
    }
}

extension DataRequest {
    static func xmlResponseSerializer() -> DataResponseSerializer<Any> {
        return DataResponseSerializer { _, response, data, error in
            // Pass through any underlying URLSession error to the .network case.
            guard error == nil else { return .failure(BackendError.network(error: error!)) }
            
            // Use Alamofire's existing data serializer to extract the data, passing the error as nil, as it has
            // already been handled.
            let result = Request.serializeResponseData(response: response, data: data, error: nil)
            
            guard case let .success(validData) = result else {
                return .failure(BackendError.dataSerialization(error: result.error! as! AFError))
            }
            
            let data = SWXMLHash.config({ (options) in
                options.shouldProcessLazily = false
            }).parse(validData)
            
            switch data {
            case .xmlError(let index):
                return .failure(BackendError.xmlSerialization(error: BackendError.objectSerialization(reason: "Cannot Serialize \(index)") ))
            default:
                let dic = data.asNSDictionary()
                
                //               let event = (((dic["weeklyevents"] as! NSDictionary)["event"] as! [AnyObject]).first!) as! NSDictionary
                return .success(dic ?? NSDictionary())
            }
            
        }
    }
    
    @discardableResult
    func responseXMLDocument(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<Any>) -> Void)
        -> Self
    {
        return response(
            queue: queue,
            responseSerializer: DataRequest.xmlResponseSerializer(),
            completionHandler: completionHandler
        )
    }
}

