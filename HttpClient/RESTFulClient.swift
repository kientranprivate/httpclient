//
//  RESTFulClient.swift
//  HttpClient
//
//  Created by Tran Kien on 7/1/17.
//  Copyright © 2017 Kien Tran. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

public protocol DefaultHeaderHelper : class {
    func defaultHeader() -> [String:String]
}

public enum ResponseDataType {
    case xml,
    json
}

public protocol HttpLogging : class {
//    func requestLog(request: URLRequest)
    func log(response: URLResponse, data: Data, request: URLRequest)
}



extension Notification.Name {
    public static let sessionDelegateDidReceiveResponse = Notification.Name("com.kientran.HttpClient.sessionDelegateDidReceiveResponse")  

}

extension SessionDelegate {
    func xxx_urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {

        self.xxx_urlSession(session, dataTask: dataTask, didReceive: data)
        
        NotificationCenter.default.post(name: NSNotification.Name.sessionDelegateDidReceiveResponse,
                                        object: self,
                                        userInfo: [
            "session": session,
            "dataTask": dataTask,
            "data": data,
            ])
    }
}

open class RESTFulClient {
   
    private static let classInit:() = {
        // do your global initialization here
        let session = SessionDelegate.self
        let originalSelector = #selector(session.urlSession(_:dataTask:didReceive:))
        let swizzledSelector = #selector(session.xxx_urlSession(_:dataTask:didReceive:))
        
        let originalMethod = class_getInstanceMethod(session, originalSelector)
        let swizzledMethod = class_getInstanceMethod(session, swizzledSelector)
        
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }()
    
    public typealias Response = (_ error:Error?,_ responseObject:Any?, _ responseData:DataResponse<Any>) -> Void
    
    open private(set) var baseURL: URL
    
    open fileprivate(set) var loggings: [HttpLogging] = []
    
    private(set) var sessionManager:SessionManager
    
    var encoding:ParameterEncoding = CustomURLEncoding()
    open private(set) var headers:[String:String] = [String:String]()
    
    open var defaultResponseDispatchQueue:DispatchQueue = DispatchQueue.main
    
    open private(set) var responseDataType:ResponseDataType = .json
    
    open weak var defaultHeaderHelper:DefaultHeaderHelper?
    
    public required init(baseURL: URL,
                         defaultHeaderHelper:DefaultHeaderHelper? = nil,
                         responseDataType:ResponseDataType = .json,
                         configuration:URLSessionConfiguration = URLSessionConfiguration.default,
                         sessionDelegate:SessionDelegate = SessionDelegate()) {
        RESTFulClient.classInit
        
        self.baseURL = baseURL
        
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.httpMaximumConnectionsPerHost = 10
        self.sessionManager = SessionManager(configuration: configuration,
                                             delegate: sessionDelegate,
                                             serverTrustPolicyManager: nil)
        

        self.responseDataType = responseDataType
        
        self.defaultHeaderHelper = defaultHeaderHelper
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.sessionDelegateDidReceiveResponse, object: sessionDelegate, queue: nil) { [weak self] (notification) in
            guard let d = notification.object as? SessionDelegate,
            let userInfo = notification.userInfo,
            let session = userInfo["session"] as? URLSession,
            let dataTask = userInfo["dataTask"] as? URLSessionDataTask,
            let data = userInfo["data"] as? Data
            else {
                return
            }
            
            guard let loggings = self?.loggings else {
                return
            }
            
            guard let response = dataTask.response,
                let request = dataTask.originalRequest else {
                    return
            }
            
            for each in loggings {
                
                each.log(response: response, data: data, request: request)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func defaultHeader() -> [String:String] {
        return self.defaultHeaderHelper?.defaultHeader() ?? [String:String]()
    }
    
    open func request(relativeURL:String,
                         method:HTTPMethod,
                         parameters: [String : Any]! = nil,
                         headers: [String : String]! = nil,
                         responseQueue:DispatchQueue? = nil,
                         block: @escaping (Response)) {
        
        let combinedHeaders = self.combineWithDefaultHeader(headers: headers)
        let absolutedURL = self.absoulteURL(relativeURL: relativeURL)
        
        let dispatchQueue = responseQueue ?? self.defaultResponseDispatchQueue
        
        let response = self.sessionManager.request(absolutedURL,
                                                   method: method,
                                                   parameters: parameters,
                                                   encoding: self.encoding,
                                                   headers: combinedHeaders)
        
        switch self.responseDataType {
        case .json:
            
            _ = response.responseJSON(queue: dispatchQueue, options: JSONSerialization.ReadingOptions()) { (dataResponse:DataResponse<Any>) in
                
                var error:Error? = nil
                var data:Any? = nil
                
                switch dataResponse.result {
                case .success(let value):
                    data = value
                case .failure(let e):
                    error = e
                }
                
                block(error, data, dataResponse)
            }
            
        case .xml:
            
            _ = response.responseXMLDocument(queue: dispatchQueue, completionHandler: { (dataResponse:DataResponse<Any>) in
                
                var error:Error? = nil
                var data:Any? = nil
                
                switch dataResponse.result {
                case .success(let value):
                    data = value
                case .failure(let e):
                    error = e
                }
                
                block(error, data, dataResponse)
            })
        }
        
    }
    
    func combineWithDefaultHeader(headers:[String:String]!) -> [String:String]{
        guard var headers = headers else {
            return self.defaultHeader()
        }
        
        for (key, value) in self.defaultHeader() {
            headers[key] = value
        }
        
        return headers
    }
    
    open func absoulteURL(relativeURL: String) -> URL {
        return self.baseURL.appendingPathComponent(relativeURL)
    }    
}

    // MARK: - Shortcuts
extension RESTFulClient {
    
    open func get(relativeURL: String,
                      parameters: [String : Any]! = nil,
                      headers: [String : String]! = nil,
                      responseQueue:DispatchQueue? = nil,
                      block: @escaping (Response)) {
        self.request(relativeURL: relativeURL, method: .get, parameters: parameters, headers: headers,responseQueue: responseQueue, block: block)
    }
    
    open func post(relativeURL: String,
                       parameters: [String : Any]! = nil,
                       headers: [String : String]! = nil,
                       responseQueue:DispatchQueue? = nil,
                       block: @escaping (Response)) {
        self.request(relativeURL: relativeURL, method: .post, parameters: parameters, headers: headers,responseQueue: responseQueue, block: block)
        
    }
    
    open func put(relativeURL: String,
                      parameters: [String : Any]! = nil,
                      headers: [String : String]! = nil,
                      responseQueue:DispatchQueue? = nil,
                      block: @escaping (Response)) {
        self.request(relativeURL: relativeURL, method: .put, parameters: parameters, headers: headers,responseQueue: responseQueue, block: block)
        
    }
    
    open func delete(relativeURL: String,
                         parameters: [String : Any]! = nil,
                         headers: [String : String]! = nil,
                         responseQueue:DispatchQueue? = nil,
                         block: @escaping (Response)) {
        self.request(relativeURL: relativeURL, method: .delete, parameters: parameters, headers: headers,responseQueue: responseQueue, block: block)
        
    }
}

// MARK: - Logging
extension RESTFulClient {
    open func append(logging: HttpLogging) {
        guard self.loggings.contains(where: { return $0 === logging }) == false else {
            return
        }
        
        self.loggings.append(logging)
    }
    
    open func remove(logging: HttpLogging) {
        guard let index = self.loggings.index( where: {$0 === logging }) else {
            return
        }
        
        self.loggings.remove(at: index)
    }
}
