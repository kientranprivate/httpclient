//
//  ConsoleHttpLogging.swift
//  HttpClient
//
//  Created by Tran Kien on 7/2/17.
//  Copyright © 2017 Kien Tran. All rights reserved.
//

import Foundation

public class ConsoleHttpLogging : HttpLogging {
    public func log(response: URLResponse, data: Data, request: URLRequest) {
        let string = String(data: data, encoding: .utf8)
        print("\(request) data \(string)")
    }
}
