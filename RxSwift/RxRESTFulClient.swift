//
//  RxRESTFulClient.swift
//  HttpClient
//
//  Created by Tran Kien on 7/3/17.
//  Copyright © 2017 Kien Tran. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import RxAlamofire
import SWXMLHash
import RxSwift

extension Reactive where Base: DataRequest {
    public func responseJSON(responseQueue: DispatchQueue? = nil, options: JSONSerialization.ReadingOptions = .allowFragments) -> Observable<(HTTPURLResponse, Any)> {
        
        return responseResult(responseSerializer: Base.jsonResponseSerializer(options: options))
    }
    
}

extension RESTFulClient : ReactiveCompatible {
    
}

extension Reactive where Base :  RESTFulClient {
    
    public func request(relativeURL:String,
                         method:HTTPMethod,
                         parameters: [String : Any]! = nil,
                         headers: [String : String]! = nil,
                         responseQueue:DispatchQueue? = nil) 
        ->  Observable<(HTTPURLResponse, Any)> {
            
            let combinedHeaders = self.base.combineWithDefaultHeader(headers: headers)
            let absolutedURL = self.base.absoulteURL(relativeURL: relativeURL)
            
            let dispatchQueue = responseQueue ?? self.base.defaultResponseDispatchQueue
            
            let observable = self.base.sessionManager.rx.request(method,
                                                                 absolutedURL, 
                                                                 parameters: parameters ?? [:],
                                                                 encoding: self.base.encoding,
                                                                 headers: combinedHeaders)
            //            .subscribeOn(scheduler)
            
            
            switch self.base.responseDataType {
            case .json:
                
                return observable.flatMap {
                    return $0.rx.responseJSON(responseQueue: dispatchQueue, options: JSONSerialization.ReadingOptions())
                    
                }  
                
                
            case .xml:
                return observable.flatMap {
                    return  $0.rx.responseResult(queue: dispatchQueue, responseSerializer: DataRequest.xmlResponseSerializer())
                }
            }
            
            
            
            
    }
    
    public func get(relativeURL: String,
                      parameters: [String : Any]! = nil,
                      headers: [String : String]! = nil,
                      responseQueue:DispatchQueue? = nil
        ) ->  Observable<(HTTPURLResponse, Any)> {
        return self.request(relativeURL: relativeURL, method: .get, parameters: parameters, headers: headers,responseQueue: responseQueue)
    }
    
    public func post(relativeURL: String,
                       parameters: [String : Any]! = nil,
                       headers: [String : String]! = nil,
                       responseQueue:DispatchQueue? = nil
        ) ->  Observable<(HTTPURLResponse, Any)> {
        return self.request(relativeURL: relativeURL, method: .post, parameters: parameters, headers: headers,responseQueue: responseQueue)
        
    }
    
    public func put(relativeURL: String,
                      parameters: [String : Any]! = nil,
                      headers: [String : String]! = nil,
                      responseQueue:DispatchQueue? = nil
        ) ->  Observable<(HTTPURLResponse, Any)> {
        return self.request(relativeURL: relativeURL, method: .put, parameters: parameters, headers: headers,responseQueue: responseQueue)
        
    }
    
    public func delete(relativeURL: String,
                         parameters: [String : Any]! = nil,
                         headers: [String : String]! = nil,
                         responseQueue:DispatchQueue? = nil) ->  Observable<(HTTPURLResponse, Any)> {
        return self.request(relativeURL: relativeURL, method: .delete, parameters: parameters, headers: headers,responseQueue: responseQueue)
        
    }
    
    //    func cleanAllRequests() {
    //        self.sessionManager.session.invalidateAndCancel()
    //    }
}
